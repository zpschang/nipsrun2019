#Install and run
```bash
cd nipsrun2019
#install anaconda for python 3.6
wget https://repo.anaconda.com/archive/Anaconda3-5.2.0-Linux-x86_64.sh
bash Anaconda3-5.2.0-Linux-x86_64.sh
#install opensim-rl following https://github.com/stanfordnmbl/osim-rl
conda create -n opensim-rl -c kidzik opensim python=3.6.1
source activate opensim-rl
conda install -c conda-forge lapack git
pip install git+https://github.com/stanfordnmbl/osim-rl.git
python -c "import opensim" #test
#install baselines dependencies
pip install -e .
#run off-policy ddpg
source activate opensim-rl
cd baselines/ddpg
nohup python main.py &> log-nipsrun &
tail -f log-nipsrun
#run on-policy trpo/ppo/acktr/upo
cd baselines
bash launch.sh opensim trpo/ppo/acktr/upo3 6.0 100
python plotvsparallel.py opensim
```

- [A2C](baselines/a2c)
- [ACER](baselines/acer)
- [ACKTR](baselines/acktr)
- [DDPG](baselines/ddpg)
- [DQN](baselines/deepq)
- [PPO1](baselines/ppo1) (Multi-CPU using MPI)
- [PPO2](baselines/ppo2) (Optimized for GPU)
- [TRPO](baselines/trpo_mpi)
