import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import os, sys


plt.ylabel('Cosine distance', fontsize=20)
plt.xlabel('Iteration', fontsize=20)
#axes = plt.gca()

files = "./fout-results/res-upo3-relu-test2_100_4.00-HalfCheetah-v1-0.txt"
fin = open(files ,'r')

tp = []
ta = []
pa = []
for l in fin:
    cos = l.split()[3].split(',')
    tp.append(float(cos[0]))
    ta.append(float(cos[1]))
    pa.append(float(cos[2]))
    
plt.title('HalfCheetah'.title(), fontsize=25)

plt.plot(range(len(tp)), tp, label='TRPO-PPO')
plt.plot(range(len(ta)), ta, label='TRPO-ACKTR')
plt.plot(range(len(pa)), pa, label='PPO-ACKTR')

print (pa)
#plt.yticks(np.arange(0, 1.0, step=0.1))
plt.ylim(ymin=0.0)
#print (np.arange(0, 1.1, step=0.1))

#plt.legend(loc='lower right', prop={'size': 20})
plt.legend(loc='lower right', fontsize=13)
plt.savefig('HalfCheetah-cosdis.pdf')


