#!/usr/bin/env python3
import argparse
import logging
import os
import gym
from baselines import logger
from baselines.common import set_global_seeds
from baselines import bench
from baselines.a2c1 import a2c1_cont
from baselines.a2c1.policies import GaussianMlpPolicy
from baselines.a2c1.value_functions import NeuralNetValueFunction
import tensorflow as tf
import numpy as np
from collections import deque
from baselines.trpo_mpi import trpo_mpi
import baselines.common.tf_util as U
from baselines.common import explained_variance, zipsame, dataset
from baselines.env import rollout, pathlength

def get_experience(env, model, nsteps, pi2):
    timesteps_this_batch = 0
    paths = []
    while True:
        path = rollout(env, model, nsteps, timesteps_this_batch, algo='ppo', pi2=pi2)
        if path["new"][-1] == False: break
        paths.append(path)
        n = pathlength(path)
        timesteps_this_batch += n
        if timesteps_this_batch >= nsteps:
            break
    obs, acs, acs_dist, rews = [], [], [], []
    news, ep_rets, ep_lens, vpreds, neglogpacs, epinfos, logps = [], [], [], [], [], [], []
    for path in paths:
        obs.append(np.squeeze(path["observation"]))
        acs.append(np.squeeze(path["action"]))
        acs_dist.append(np.squeeze(path["action_dist"]))
        rews.append(np.squeeze(path["reward"]))
        news.append(np.squeeze(path["new"]))
        ep_rets.append(path["ep_rets"])
        ep_lens.append(path["ep_lens"])
        vpreds.append(path["vpred"])
        neglogpacs.append(np.squeeze(path["neglogpacs"]))
        epinfos.append(path["epinfos"][0])
        logps.append(np.squeeze(path["logp"]))
    obs = np.concatenate(obs)
    acs = np.concatenate(acs)
    acs_dist = np.concatenate(acs_dist)
    rews = np.concatenate(rews)
    news = np.concatenate(news)
    ep_rets = np.concatenate(ep_rets)
    ep_lens = np.concatenate(ep_lens)
    vpreds = np.concatenate(vpreds)
    neglogpacs = np.concatenate(neglogpacs)
    #epinfos = np.concatenate(epinfos)
    logps = np.concatenate(logps)

    obs = np.squeeze(obs)
    acs = np.squeeze(acs)
    acs_dist = np.squeeze(acs_dist)
    rews = np.squeeze(rews)
    news = np.squeeze(news)
    ep_rets = np.squeeze(ep_rets)
    ep_lens = np.squeeze(ep_lens)
    vpreds = np.squeeze(vpreds)
    neglogpacs = np.squeeze(neglogpacs)
    logps = np.squeeze(logps)

    seg = {"ob" : obs, "rew" : rews, "vpred" : vpreds, "new" : news,
           "ac" : acs, "ep_rets" : ep_rets, "ep_lens" : ep_lens,
           "action_dist" : acs_dist, "reward": rews, "observation": obs,
           "paths": paths, "timesteps_this_batch": timesteps_this_batch,
           "neglogpacs": neglogpacs, "logp": logps, "epinfos": epinfos}
    return seg

def train(env_id, num_timesteps, seed):
    env=gym.make(env_id)
    #env = bench.Monitor(env, logger.get_dir() and os.path.join(logger.get_dir(), str(rank)))
    env = bench.Monitor(env, logger.get_dir(), allow_early_resets=True)
    set_global_seeds(seed)
    env.seed(seed)
    gym.logger.setLevel(logging.WARN)

    with tf.Session(config=tf.ConfigProto()):
        #####
        timesteps_per_batch = 2500
        gamma = 0.99
        lam = 0.98
        from baselines.ppo1.mlp_policy import MlpPolicy as trpoMlpPolicy
        def policy_fn(name, ob_space, ac_space):
            return trpoMlpPolicy(name=name, ob_space=env.observation_space, ac_space=env.action_space,
                hid_size=64, num_hid_layers=2)
        trpoLearn = trpo_mpi.Learn(env, policy_fn, timesteps_per_batch=timesteps_per_batch, max_kl=0.01, cg_iters=10, cg_damping=0.1,
            max_timesteps=num_timesteps, gamma=0.99, lam=0.98, vf_iters=5, vf_stepsize=1e-3)
        #print (trpoLearn.pi.get_variables())
        #print ("---------------------------")
        #print (trpoLearn.oldpi.get_variables())
        pi_var, oldpi_var = [], []
        for var in tf.trainable_variables():
            if "vf" not in var.name and "old" not in var.name:
                pi_var.append(var)
            if "old" in var.name and "vf" not in var.name and "obfilter" not in var.name:
                oldpi_var.append(var)
        print (pi_var)
        print ("---------------------------")
        print (oldpi_var)
        pi_reset = U.function([],[], updates=[tf.assign(newv, oldv)
            for (oldv, newv) in zipsame(oldpi_var, pi_var)])
        assign_old_eq_new = U.function([],[], updates=[tf.assign(oldv, newv)
            for (oldv, newv) in zipsame(oldpi_var, pi_var)])
        #####
        print ("TRPO==========================")
        print (tf.trainable_variables())


        #####PPO
        from baselines.ppo2 import ppo2
        from baselines.ppo2.policies import MlpPolicy
        policy = MlpPolicy
        ppo2Learn = ppo2.Learn(policy=policy, env=env, nsteps=2048, nminibatches=32,
            lam=0.95, gamma=0.99, noptepochs=10, log_interval=1,
            ent_coef=0.0,
            lr=3e-4,
            cliprange=0.2,
            total_timesteps=num_timesteps)
        print ("PPO==========================")
        print (tf.trainable_variables())
        #####

        ob_dim = env.observation_space.shape[0]
        ac_dim = env.action_space.shape[0]
        with tf.variable_scope("vfelu"):
            vf = NeuralNetValueFunction(ob_dim, ac_dim)
        with tf.variable_scope("pi", reuse=tf.AUTO_REUSE):
            policy = GaussianMlpPolicy(ob_dim, ac_dim)
        print ("A2C1==========================")
        print (tf.trainable_variables())

        a2c1Learn = a2c1_cont.Learn(env, policy=policy, vf=vf,
            gamma=0.99, lam=0.97, timesteps_per_batch=timesteps_per_batch,
            desired_kl=0.002,
            num_timesteps=num_timesteps, animate=False)

        total_timesteps = 0
        rewbuffer = deque(maxlen=100)
        fout = open('res-a2c1-relu-%s-%d.txt'%(env_id, seed), 'a+')
        sess = tf.get_default_session()
        U.initialize()
        def get_pi_var():
            pi_var_list = []
            for var in tf.trainable_variables():
                if "vf" not in var.name and "old" not in var.name:
                    pi_var_list.append(var)
            return pi_var_list
        print( get_pi_var() )
        def getmap():
            pi_var_list = get_pi_var()
            pi_var_names = [v.name for v in pi_var_list]
            pi_var_values = sess.run(pi_var_names)
            pi_var_ = {}
            for i, p in enumerate(pi_var_list):
                pi_var_[p.name] = np.copy(pi_var_values[i])
            return pi_var_

        pi_var_gradient = {}

        while total_timesteps < num_timesteps:
            seg = get_experience(env, ppo2Learn.model, timesteps_per_batch, policy)
            #seg = get_experience(env, policy, timesteps_per_batch)
            #add_vtarg_and_adv(seg, gamma, lam)

            pi_var_before = getmap()
            assign_old_eq_new()  #old <- new, before any update
            timesteps_this_batch, raw_returns = a2c1Learn.update_once(seg)
            pi_var_after = getmap()
            for k,v in pi_var_after.items():
                pi_var_gradient[k] = np.copy(pi_var_after[k] - pi_var_before[k])
            for k,v in pi_var_before.items():
                gradient = np.zeros_like(pi_var_before[k])
                gradient = pi_var_gradient[k]
                pi_var_before[k] = pi_var_before[k] + gradient
            pi_reset() #old -> new, after each update
            pi_update = U.function([],[], updates=[tf.assign(p, pi_var_before[p.name])
                for i,p in enumerate(get_pi_var())])
            pi_update()

            total_timesteps += timesteps_this_batch
            rewbuffer.extend(raw_returns)
            fout.write("%d %d\n" % (total_timesteps, np.mean(rewbuffer)))
            fout.flush()

        env.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Run Mujoco benchmark.')
    parser.add_argument('--seed', help='RNG seed', type=int, default=0)
    parser.add_argument('--env', help='environment ID', type=str, default="HalfCheetah-v1")
    parser.add_argument('--num-timesteps', type=int, default=int(2e6))
    args = parser.parse_args()
    logger.configure()
    train(args.env, num_timesteps=args.num_timesteps, seed=args.seed)
