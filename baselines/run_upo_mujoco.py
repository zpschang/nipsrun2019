#!/usr/bin/env python
import sys
sys.path = ['../'] + sys.path
import argparse
from baselines import bench, logger
from collections import deque
import numpy as np
from baselines.trpo_mpi import trpo_mpi
import baselines.common.tf_util as U
from baselines.common import explained_variance, zipsame, dataset
from baselines.env import rollout, pathlength
#from baselines.trpo_mpi.run_mujoco import add_vtarg_and_adv
import os
import time
import scipy
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

def add_vtarg_and_adv(seg, gamma, lam):
    new = np.append(seg["new"], 0) # last element is only used for last vtarg, but we already zeroed it if last new = 1
    vpred = np.append(seg["vpred"], 0)
    T = len(seg["rew"])
    seg["adv"] = gaelam = np.empty(T, 'float32')
    rew = seg["rew"]
    lastgaelam = 0
    for t in reversed(range(T)):
        nonterminal = 1-new[t]
        delta = rew[t] + gamma * vpred[t+1] * nonterminal - vpred[t]
        gaelam[t] = lastgaelam = delta + gamma * lam * nonterminal * lastgaelam
    seg["tdlamret"] = seg["adv"] + seg["vpred"]

def get_experience(env, model, nsteps, tpaths=[]):
    timesteps_this_batch = 0
    paths = []
    while True:
        path = rollout(env, model, nsteps, timesteps_this_batch, algo='ppo')
        if path["new"][-1] == False: break
        paths.append(path)
        n = pathlength(path)
        timesteps_this_batch += n
        if timesteps_this_batch >= nsteps:
            break
    paths.extend(tpaths)
    obs, acs, acs_dist, rews = [], [], [], []
    news, ep_rets, ep_lens, vpreds, neglogpacs, epinfos, logps = [], [], [], [], [], [], []
    raw_ret100, raw_ret200, raw_ret300, raw_ret150 = [], [], [], []
    for path in paths:
        raw_ret100.append(path["raw_ret100"])
        raw_ret150.append(path["raw_ret150"])
        raw_ret200.append(path["raw_ret200"])
        raw_ret300.append(path["raw_ret300"])
        obs.append(np.squeeze(path["observation"]))
        acs.append(np.squeeze(path["action"]))
        acs_dist.append(np.squeeze(path["action_dist"]))
        rews.append(np.squeeze(path["reward"]))
        news.append(np.squeeze(path["new"]))
        ep_rets.append(path["ep_rets"])
        ep_lens.append(path["ep_lens"])
        vpreds.append(path["vpred"])
        neglogpacs.append(np.squeeze(path["neglogpacs"]))
        epinfos.append(path["epinfos"][0])
        logps.append(np.squeeze(path["logp"]))
    obs = np.concatenate(obs)
    acs = np.concatenate(acs)
    acs_dist = np.concatenate(acs_dist)
    rews = np.concatenate(rews)
    news = np.concatenate(news)
    ep_rets = np.concatenate(ep_rets)
    ep_lens = np.concatenate(ep_lens)
    vpreds = np.concatenate(vpreds)
    neglogpacs = np.concatenate(neglogpacs)
    #epinfos = np.concatenate(epinfos)
    logps = np.concatenate(logps)

    obs = np.squeeze(obs)
    if len(obs.shape) == 1: obs = np.reshape(obs, (-1, 1))
    acs = np.squeeze(acs)
    if len(acs.shape) == 1: acs = np.reshape(acs, (-1, 1))
    acs_dist = np.squeeze(acs_dist)
    rews = np.squeeze(rews)
    news = np.squeeze(news)
    ep_rets = np.squeeze(ep_rets)
    ep_lens = np.squeeze(ep_lens)
    vpreds = np.squeeze(vpreds)
    neglogpacs = np.squeeze(neglogpacs)
    logps = np.squeeze(logps)

    seg = {"ob" : obs, "rew" : rews, "vpred" : vpreds, "new" : news,
           "ac" : acs, "ep_rets" : ep_rets, "ep_lens" : ep_lens,
           "action_dist" : acs_dist, "reward": rews, "observation": obs,
           "paths": paths, "timesteps_this_batch": timesteps_this_batch,
           "neglogpacs": neglogpacs, "logp": logps, "epinfos": epinfos,
           "raw_ret100": raw_ret100, "raw_ret150": raw_ret150, "raw_ret200": raw_ret200, "raw_ret300": raw_ret300}
    return seg

def merge_experience(seg, tpaths):
    for path in tpaths:
        seg["ob"] = np.concatenate((seg["ob"], path["observation"]))
        seg["observation"] = np.concatenate((seg["observation"], path["observation"]))
        seg["rew"] = np.concatenate((seg["rew"], path["reward"]))
        seg["reward"] = np.concatenate((seg["reward"], path["reward"]))
        #print (seg["vpred"].shape)
        #print (path["vpred"].shape)
        seg["vpred"] = np.concatenate((seg["vpred"], np.reshape(path["vpred"], (-1, ))))
        seg["new"] = np.concatenate((seg["new"], path["new"]))
        seg["ac"] = np.concatenate((seg["ac"], path["action"]))
        seg["ep_rets"] = np.concatenate((seg["ep_rets"], path["ep_rets"]))
        seg["ep_lens"] = np.concatenate((seg["ep_lens"], path["ep_lens"]))
        seg["action_dist"] = np.concatenate((seg["action_dist"], path["action_dist"]))
        #print (seg["neglogpacs"].shape)
        #print (np.array(path["neglogpacs"]).shape)
        seg["neglogpacs"] = np.concatenate((seg["neglogpacs"], np.reshape(np.array(path["neglogpacs"]), (-1,))))
        #print (seg["logp"].shape)
        #print (path["logp"].shape)
        seg["logp"] = np.concatenate((seg["logp"], np.reshape(path["logp"], (-1,))))
        seg["epinfos"] = np.concatenate((seg["epinfos"], path["epinfos"]))
        seg["paths"].append(path)
        seg["timesteps_this_batch"] += pathlength(path)

def softmax(x, tal):
    return np.exp(x/tal) / np.sum(np.exp(x/tal), axis=0)
def sigmoid(x):
    return 1. / (1.+np.exp(-x))

def train(env_id, num_timesteps, seed, method, tal, test=False, ntest=100, bandit=False, alpha=0.01, random=False):
    from baselines.common import set_global_seeds
    from baselines.common.vec_env.vec_normalize import VecNormalize
    from baselines.ppo2 import ppo2
    from baselines.ppo2.policies import MlpPolicy
    import gym
    import tensorflow as tf
    from baselines.common.vec_env.dummy_vec_env import DummyVecEnv
    from osim.env import ProstheticsEnv
    ncpu = 1
    config = tf.ConfigProto(#allow_soft_placement=True,
                            #intra_op_parallelism_threads=ncpu,
                            #inter_op_parallelism_threads=ncpu,
                            device_count = {'GPU': 0})
    #config.gpu_options.per_process_gpu_memory_fraction = 0.01
    tf.Session(config=config).__enter__()
    def make_env():
        #env = gym.make(env_id)
        env = ProstheticsEnv(visualize=False)
        env = bench.Monitor(env, logger.get_dir(), allow_early_resets=True)
        return env
    env = make_env()
    render_env = make_env()
    #env = DummyVecEnv([make_env])
    #env = VecNormalize(env)

    #####TRPO
    #NOTE
    timesteps_per_batch = 1024
    gamma = 0.99
    lam = 0.98
    from baselines.ppo1.mlp_policy import MlpPolicy as trpoMlpPolicy
    def policy_fn(name, ob_space, ac_space):
        ob_dim = 160 if 'opensim' in env_id else None
        return trpoMlpPolicy(name=name, ob_space=env.observation_space, ac_space=env.action_space,
            hid_size=64, num_hid_layers=2, ob_dim=ob_dim)
    trpoLearn = trpo_mpi.Learn(env, policy_fn, timesteps_per_batch=timesteps_per_batch, max_kl=0.01, cg_iters=10, cg_damping=0.1,
        max_timesteps=num_timesteps, gamma=0.99, lam=0.98, vf_iters=5, vf_stepsize=1e-3)
    #print (trpoLearn.pi.get_variables())
    #print ("---------------------------")
    #print (trpoLearn.oldpi.get_variables())
    pi_var, oldpi_var = [], []
    for var in tf.trainable_variables():
        if "vf" not in var.name and "old" not in var.name:
            pi_var.append(var)
        if "old" in var.name and "vf" not in var.name and "obfilter" not in var.name:
            oldpi_var.append(var)
    print (pi_var)
    print ("---------------------------")
    print (oldpi_var)
    pi_reset = U.function([],[], updates=[tf.assign(newv, oldv)
        for (oldv, newv) in zipsame(oldpi_var, pi_var)])
    assign_old_eq_new = U.function([],[], updates=[tf.assign(oldv, newv)
        for (oldv, newv) in zipsame(oldpi_var, pi_var)])
    print ("==========================")
    print (tf.trainable_variables())

    #####PPO
    set_global_seeds(seed)
    policy = MlpPolicy
    ob_dim = 160 if 'opensim' in env_id else None
    ppo2Learn = ppo2.Learn(policy=policy, env=env, nsteps=2048, nminibatches=32,
        lam=0.95, gamma=0.99, noptepochs=10, log_interval=1,
        ent_coef=0.0,
        lr=3e-4,
        cliprange=0.2,
        total_timesteps=num_timesteps, ob_dim=ob_dim)

    #####ACKTR
    from baselines.acktr import acktr_cont
    from baselines.acktr.policies import GaussianMlpPolicy
    from baselines.acktr.value_functions import NeuralNetValueFunction
    ob_dim = 160 if 'opensim' in env_id else env.observation_space.shape[0]
    ac_dim = env.action_space.shape[0]
    with tf.variable_scope("vfelu"):
        vf = NeuralNetValueFunction(ob_dim, ac_dim)
    with tf.variable_scope("pi", reuse=tf.AUTO_REUSE):
        policy = GaussianMlpPolicy(ob_dim, ac_dim)
    print ("ACKTR==========================")
    print (tf.trainable_variables())

    acktrLearn = acktr_cont.Learn(env, policy=policy, vf=vf,
        gamma=0.99, lam=0.97, timesteps_per_batch=timesteps_per_batch,
        desired_kl=0.002,
        num_timesteps=num_timesteps, animate=False)

    #####A2C
    from baselines.a2c1 import a2c1_cont
    from baselines.a2c1.policies import GaussianMlpPolicy as a2c1MlpPolicy
    from baselines.a2c1.value_functions import NeuralNetValueFunction
    #a2cLearn = a2c1_cont.Learn(a2c1MlpPolicy, env, seed, nsteps=2048, total_timesteps=num_timesteps, lrschedule="linear")
    with tf.variable_scope("vfelu_a2c"):
        vf_a2c = NeuralNetValueFunction(ob_dim, ac_dim)
    a2cLearn = a2c1_cont.Learn(env, policy=policy, vf=vf_a2c,
        gamma=0.99, lam=0.97, timesteps_per_batch=timesteps_per_batch,
        desired_kl=0.002,
        num_timesteps=num_timesteps, animate=False)

    #Define
    total_timesteps = 0
    total_timesteps_test = 0
    rewbuffer = deque(maxlen=100) # rolling buffer for episode rewards
    sess = tf.get_default_session()
    U.initialize()
    def get_pi_var():
        pi_var_list = []
        for var in tf.trainable_variables():
            if "vf" not in var.name and "old" not in var.name:
                pi_var_list.append(var)
        return pi_var_list
    print( get_pi_var() )
    def getmap():
        pi_var_list = get_pi_var()
        pi_var_names = [v.name for v in pi_var_list]
        pi_var_values = sess.run(pi_var_names)
        pi_var_ = {}
        for i, p in enumerate(pi_var_list):
            pi_var_[p.name] = np.copy(pi_var_values[i])
        return pi_var_

    weight = [1.0]
    if method == 'trpo':
        learnMethod = [trpoLearn]
    if method == 'ppo':
        learnMethod = [ppo2Learn]
    if method == 'acktr':
        learnMethod = [acktrLearn]
    if method == 'a2c':
        learnMethod = [a2cLearn]
    if method == 'trpo_ppo':
        learnMethod = [trpoLearn, ppo2Learn]
    if method == 'trpo_acktr':
        learnMethod = [trpoLearn, acktrLearn]
    if method == 'ppo_acktr':
        learnMethod = [ppo2Learn, acktrLearn]
    if method == 'upo3' or method == 'avg3':
        learnMethod = [trpoLearn, ppo2Learn, acktrLearn]
    if method == 'upo4':
        learnMethod = [trpoLearn, ppo2Learn, acktrLearn, a2cLearn]
    #NOTE
    teststep = ntest
    nmethods = len(learnMethod)
    weight = [1.0 / nmethods for _ in range(nmethods)]
    if bandit:
        ucb = [0 for _ in range(nmethods)]
        Q = [0 for _ in range(nmethods)]
        N = [1 for _ in range(nmethods)]
    if not test:
        if not os.path.exists("./fout-results"):
            os.makedirs("./fout-results")
        if bandit == True:
            fout = open('fout-results/res-%s-relu-test2_%d_%.2f-%s-%s_bandit_%.2f.txt' % (method, teststep, tal, env_id, seed, alpha), 'a+')
        elif random == True:
            fout = open('fout-results/res-%s-relu-test2_%d_%.2f-%s-%s_random.txt' % (method, teststep, tal, env_id, seed), 'a+')
        else:
            fout = open('fout-results/res-%s-relu-test2_%d_%.2f-%s-%s.txt' % (method, teststep, tal, env_id, seed), 'a+')

    pi_var_gradient = [{} for _ in range(len(learnMethod))]

    test_F = [0. for _ in range(len(learnMethod))]
    meanscore_each_update, running_delta = [], []

    tpaths = []

    ## Get all trainable variables
    if not os.path.exists("./models"):
        os.makedirs("./models")
    trainable_variables = tf.trainable_variables()
    saver = tf.train.Saver(trainable_variables)
    best_mean = -10000
    if test:
        saver.restore(sess, './models/model-%s_%s_%d' % (method, env_id, seed))
        for _ in range(10):
            tpath = rollout(render_env, ppo2Learn.model, 1000, 0, algo='ppo', render=True)
    update_t = 0
    while total_timesteps < num_timesteps and not test:
        update_t += 1
        seg = get_experience(env, ppo2Learn.model, timesteps_per_batch)
        add_vtarg_and_adv(seg, gamma, lam)
        tpaths = []

        pi_var_before = getmap()
        assign_old_eq_new()  #old <- new, before any update

        flat_gradients = [[] for _ in range(len(learnMethod))]
        for i in range(len(learnMethod)):
            #update
            timesteps_this_batch, raw_returns = learnMethod[i].update_once(seg)
            #save gradient
            pi_var_after = getmap()
            for k,v in pi_var_after.items():
                pi_var_gradient[i][k] = np.copy(pi_var_after[k] - pi_var_before[k])
                flat_gradients[i].append(np.copy(pi_var_gradient[i][k]).flatten())
            flat_gradients[i] = np.concatenate(flat_gradients[i])
            #test
            if nmethods > 1:
                test_rets = []
                for _ in range(1):
                    #tpath = rollout(env, ppo2Learn.model, timesteps_per_batch, 0, algo='ppo')
                    #NOTE
                    tpath = rollout(env, ppo2Learn.model, teststep, 0, algo='ppo')
                    tpaths.append(tpath)
                    #test_rets.append(tpath["epinfos"][0]['r'])
                    test_rets.append(tpath["raw_ret"+str(teststep)])
                test_F[i] = np.mean(test_rets)
            #reset
            pi_reset() #old -> new, after each update

        cos_distances = []
        cos_distances_meta = []
        gradient_norms = []
        for i in range(nmethods):
            for j in range(i+1, nmethods):
                cos_distance = scipy.spatial.distance.cosine(flat_gradients[i], flat_gradients[j])
                cos_distances.append(np.round_(cos_distance, 3))
            gradient_norms.append(np.round_(np.linalg.norm(flat_gradients[i], 2), 3))
        #np.std(cos_distances)

        meanscore_each_update.append(np.mean(seg["raw_ret"+str(teststep)]))
        rets0 = np.mean(seg["raw_ret"+str(teststep)])
        #meanscore_each_update.append(np.mean(raw_returns))
        if len(meanscore_each_update) > 1 and nmethods > 1:
            running_delta.append(meanscore_each_update[-1] - meanscore_each_update[-2])
            delta_mean, delta_std = np.mean(running_delta), np.std(running_delta)
            print ("test_F::::::", test_F)
            print ("test_F-rets0::::::", test_F-rets0)
            print ("delta_dist::::::", delta_mean, delta_std)
            test_F = ((test_F - rets0) - delta_mean) / (delta_std if delta_std != 0 else 1.0)
            if tal == 1000:
                weight = [1./nmethods for _ in range(nmethods)]
            elif tal == 0.01:
                idx_best = np.argmax(test_F)
                weight = [1 if i == idx_best else 0 for i in range(nmethods)]
            else:
                weight = softmax(test_F, tal)
            print ("weight::::::", weight)

            if bandit == True:
                ucb = [Q[i] + np.sqrt(2*np.log(update_t) / N[i]) for i in range(nmethods)]
                print ("bandit-ucb:", ucb)
                a = np.argmax(ucb)
                print (a)
                weight = [1 if i == a else 0 for i in range(nmethods)]
                print (weight)
                reward = sigmoid(test_F)
                print ("reward-sigmoid:", reward)
                Q[a] = (1-alpha) * Q[a] + alpha * reward[a]
                N[a] += 1

            if random == True:
                action = np.random.randint(0, nmethods)
                weight = [1 if i == action else 0 for i in range(nmethods)]

        ##### Use Testing Data
        '''
        pi_var_gradient1 = [{} for _ in range(len(learnMethod))]
        merge_experience(seg, tpaths)
        add_vtarg_and_adv(seg, gamma, lam)
        pi_var_before = getmap()
        assign_old_eq_new()  #old <- new, before any update
        for i in range(len(learnMethod)):
            #update
            timesteps_this_batch, raw_returns = learnMethod[i].update_once(seg)
            #save gradient
            pi_var_after = getmap()
            for k,v in pi_var_after.items():
                pi_var_gradient1[i][k] = np.copy(pi_var_after[k] - pi_var_before[k])
            #reset
            pi_reset() #old -> new, after each update
        #####
        '''
        #NOTE
        if nmethods > 1:
            #timesteps_this_batch += np.sum([len(p["observation"]) for p in tpaths])
            print ("testing length:", [len(p["observation"]) for p in tpaths])

        flat_meta = []
        for k,v in pi_var_before.items():
            meta_gradient = np.zeros_like(pi_var_before[k])
            for i in range(len(learnMethod)):
                #meta_gradient += (pi_var_gradient[i][k] + pi_var_gradient1[i][k]) / 2.0 * weight[i]
                #NOTE
                meta_gradient += pi_var_gradient[i][k] * weight[i]
            pi_var_before[k] = pi_var_before[k] + meta_gradient
            flat_meta.append(meta_gradient.flatten())
        flat_meta = np.concatenate(flat_meta)
        for i in range(nmethods):
            cos_distance = scipy.spatial.distance.cosine(flat_gradients[i], flat_meta)
            cos_distances_meta.append(np.round_(cos_distance, 3))
        gradient_norms.append(np.round_(np.linalg.norm(flat_meta, 2), 3))

        pi_update = U.function([],[], updates=[tf.assign(p, pi_var_before[p.name])
            for i,p in enumerate(get_pi_var())])
        pi_update()

        total_timesteps += timesteps_this_batch
        total_timesteps_test += timesteps_this_batch + np.sum([len(p["observation"]) for p in tpaths])
        rewbuffer.extend(raw_returns)
        if bandit == True:
            fout.write('%d %d %s %s %s %s\n' % (total_timesteps, np.mean(rewbuffer), ','.join(map(str, weight)), ','.join(map(str, ucb)), ','.join(map(str, Q)), ','.join(map(str, N))))
        elif random == True:
            fout.write('%d %d %s\n' % (total_timesteps, np.mean(rewbuffer), ','.join(map(str, weight))))
        else:
            fout.write('%d %d %s %s %s %s %d\n' % (total_timesteps, np.mean(rewbuffer), ','.join(map(str, weight)), ','.join(map(str, cos_distances)), ','.join(map(str, gradient_norms)), ','.join(map(str, cos_distances_meta)), total_timesteps_test))
        print (np.mean(rewbuffer))
        if np.mean(rewbuffer) > best_mean and not test:
            time1 = time.time()
            best_mean = np.mean(rewbuffer)
            saver.save(sess, './models/model-%s_%s_%d' % (method, env_id, seed))
            time2 = time.time()
            print ("Saved:", time2-time1)

        fout.flush()
    env.close()
    render_env.close()


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--env', help='environment ID', default='opensim')
    parser.add_argument('--seed', help='RNG seed', type=int, default=0)
    parser.add_argument('--tal', help='Softmax tal', type=float, default=6.0)
    parser.add_argument('--num-timesteps', type=int, default=int(10e6))
    parser.add_argument('--method', help='Method', default='upo3')
    parser.add_argument('--test', help='IfTest', type=bool, default=False)
    parser.add_argument('--ntest', type=int, default=int(100))
    parser.add_argument('--bandit', help='Use bandit', type=bool, default=False)
    parser.add_argument('--random', help='Use random bandit', type=bool, default=False)
    parser.add_argument('--alpha', help='Bandit alpha', type=float, default=0.01)
    args = parser.parse_args()
    logger.configure()
    train(args.env, num_timesteps=args.num_timesteps, seed=args.seed, method=args.method, tal=args.tal, test=args.test, ntest=args.ntest, bandit=args.bandit, alpha=args.alpha, random=args.random)


if __name__ == '__main__':
    main()

