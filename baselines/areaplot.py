import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import sys, os
import seaborn as sns
import pandas as pd


plt.ylabel('TRPO   --------------------->   PPO')
plt.xlabel('Timestep x 1e6')
axes = plt.gca()

def moving_avg(a, n=3):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n-1:] / n

files, files1, files2, files3 = [],[],[],[]
labels, labels1, labels2, labels3= [],[],[],[]
game = sys.argv[1] + '-v1'
#alpha = sys.argv[2]
#win = sys.argv[3]
#c = sys.argv[4] if len(sys.argv) >= 5 else ""
#nmethod = sys.argv[5]
nmethod = '3'

for i,f in enumerate(os.listdir("./")):
    if 'txt' not in f: continue
    #if 'upo' in f and game in f and '0' in f:
    if 'upo3' in f and game in f and '0' in f:
        files.append(f)
        labels.append('upo3-'+str(i))
print (files)

plt.title(game.title())
agent_used = [[] for _ in range(len(files))]


def fill(files_, color_, color1_, label_):
    maxs = [[], [], []]
    mins = [[], [], []]
    avgs = [[], [], []]
    alls = []
    leng = 2e7
    area = {}
    for i in range(int(nmethod)):
        area[i] = []
    for i,f in enumerate(files_):
        fin = open(f, 'r')
        steps = []
        scores = []
        agent = []
        for l in fin:
            t = l.split()
            step = int(t[0])
            steps.append(step)
            per = list(map(float, t[2].split(',')))
            print (per)
            for j in range(int(nmethod)): area[j].append(per[j])
        #plt.plot(steps[10-1:], moving_avg(scores, 10), label=f.split('-')[0])
        ##plt.plot(steps[10-1:], moving_avg(scores, 10), label=labels[i])
        alls.append([steps, agent])
        leng = min(len(steps), leng)

    n = 2
    for j in range(int(nmethod)):
        area[j] = moving_avg(area[j], n)
        print (len(area[j]))

    #leng = 2e7
    #for i in range(int(nmethod)): leng = min(leng, len(area[i]))

    #for i in range(int(nmethod)):
    #    area[i] = area[i][:leng]

    #data = pd.DataFrame(area, index=range(0, leng))
    steps = (np.array(steps[:-(n-1)])) / 1e6
    data = pd.DataFrame(area, index=steps)
    data_perc = data.divide(data.sum(axis=1), axis=0)

    plt.stackplot(steps,  data_perc[0],  data_perc[1],  data_perc[2], labels=['trpo','ppo','acktr'])
    #plt.stackplot(steps,  data_perc[0],  data_perc[1],  labels=['trpo','ppo'])
    plt.legend(loc='upper left')
    plt.margins(0,0)
    #plt.title('100 % stacked area chart')


fill(files, 'salmon', 'red', 'upo')
#fill(files1, 'grey', 'black', 'random')
#fill(files3, 'cyan', 'blue', 'ppo')
#fill(files2, 'lightgreen', 'green', 'trpo')


print (agent_used[0].count(0), agent_used[0].count(1))
plt.legend(loc='lower right')
plt.savefig(game + '-area3.png')


