#!/usr/bin/env python3
# noinspection PyUnresolvedReferences
import mujoco_py # Mujoco must come before other imports. https://openai.slack.com/archives/C1H6P3R7B/p1492828680631850
from mpi4py import MPI
from baselines.common import set_global_seeds
import os.path as osp
import gym
import logging
from baselines import logger
from baselines.ppo1.mlp_policy import MlpPolicy
from baselines.common.mpi_fork import mpi_fork
from baselines import bench
from baselines.trpo_mpi import trpo_mpi
from collections import deque
import numpy as np
from baselines.env import rollout, pathlength
import tensorflow as tf
from baselines.common import explained_variance, zipsame, dataset
import sys

def add_vtarg_and_adv(seg, gamma, lam):
    #new = np.append(seg["new"], 0) # last element is only used for last vtarg, but we already zeroed it if last new = 1
    new = seg["new"]
    #vpred = np.append(seg["vpred"], seg["nextvpred"])
    vpred = np.append(seg["vpred"], 0)
    T = len(seg["rew"])
    seg["adv"] = gaelam = np.empty(T, 'float32')
    rew = seg["rew"]
    lastgaelam = 0
    for t in reversed(range(T)):
        nonterminal = 1-new[t]
        delta = rew[t] + gamma * vpred[t+1] * nonterminal - vpred[t]
        gaelam[t] = lastgaelam = delta + gamma * lam * nonterminal * lastgaelam
    seg["tdlamret"] = seg["adv"] + seg["vpred"]
def get_experience(env, model, nsteps):
    timesteps_this_batch = 0
    paths = []
    while True:
        path = rollout(env, model, nsteps, timesteps_this_batch, algo='trpo')
        if path["new"][-1] == False: break
        paths.append(path)
        n = pathlength(path)
        timesteps_this_batch += n
        if timesteps_this_batch >= nsteps:
            break
    obs, acs, acs_dist, rews = [], [], [], []
    news, ep_rets, ep_lens, vpreds, neglogpacs, epinfos, logps = [], [], [], [], [], [], []
    for path in paths:
        obs.append(np.squeeze(path["observation"]))
        acs.append(np.squeeze(path["action"]))
        acs_dist.append(np.squeeze(path["action_dist"]))
        rews.append(np.squeeze(path["reward"]))
        news.append(np.squeeze(path["new"]))
        ep_rets.append(path["ep_rets"])
        ep_lens.append(path["ep_lens"])
        vpreds.append(path["vpred"])
        neglogpacs.append(np.squeeze(path["neglogpacs"]))
        epinfos.append(np.squeeze(path["epinfos"]))
        logps.append(np.squeeze(path["logp"]))
    obs = np.concatenate(obs)
    acs = np.concatenate(acs)
    acs_dist = np.concatenate(acs_dist)
    rews = np.concatenate(rews)
    news = np.concatenate(news)
    ep_rets = np.concatenate(ep_rets)
    ep_lens = np.concatenate(ep_lens)
    vpreds = np.concatenate(vpreds)
    neglogpacs = np.concatenate(neglogpacs)
    #epinfos = np.concatenate(epinfos)
    logps = np.concatenate(logps)

    obs = np.squeeze(obs)
    acs = np.squeeze(acs)
    acs_dist = np.squeeze(acs_dist)
    rews = np.squeeze(rews)
    news = np.squeeze(news)
    ep_rets = np.squeeze(ep_rets)
    ep_lens = np.squeeze(ep_lens)
    vpreds = np.squeeze(vpreds)
    neglogpacs = np.squeeze(neglogpacs)
    epinfos = np.squeeze(epinfos)
    logps = np.squeeze(logps)

    seg = {"ob" : obs, "rew" : rews, "vpred" : vpreds, "new" : news,
           "ac" : acs, "ep_rets" : ep_rets, "ep_lens" : ep_lens,
           "action_dist" : acs_dist, "reward": rews, "observation": obs,
           "paths": paths, "timesteps_this_batch": timesteps_this_batch,
           "neglogpacs": neglogpacs, "logp": logps}
    return seg

def train(env_id, num_timesteps, seed):
    import baselines.common.tf_util as U
    sess = U.single_threaded_session()
    sess.__enter__()

    rank = MPI.COMM_WORLD.Get_rank()
    if rank != 0:
        logger.set_level(logger.DISABLED)
    workerseed = seed + 10000 * MPI.COMM_WORLD.Get_rank()
    set_global_seeds(workerseed)

    #def make_env():
    #    env = gym.make(env_id)
    #    env = bench.Monitor(env, logger.get_dir())
    #    return env
    #env = make_env()
    #env = VecNormalize(env, ret=False, clipob=5.)

    env = gym.make(env_id)
    def policy_fn(name, ob_space, ac_space):
        return MlpPolicy(name=name, ob_space=env.observation_space, ac_space=env.action_space,
            hid_size=64, num_hid_layers=2)
    env = bench.Monitor(env, logger.get_dir() and osp.join(logger.get_dir(), str(rank)), allow_early_resets=True)
    env.seed(workerseed)
    gym.logger.setLevel(logging.WARN)

    #trpo_mpi.learn(env, policy_fn, timesteps_per_batch=1024, max_kl=0.01, cg_iters=10, cg_damping=0.1,
    #    max_timesteps=num_timesteps, gamma=0.99, lam=0.98, vf_iters=5, vf_stepsize=1e-3,
    #    env_id=env_id, seed=seed)
    timesteps_per_batch = 2048
    gamma = 0.99
    lam = 0.98
    trpoLearn = trpo_mpi.Learn(env, policy_fn, timesteps_per_batch=timesteps_per_batch, max_kl=0.01, cg_iters=10, cg_damping=0.1,
        max_timesteps=num_timesteps, gamma=0.99, lam=0.98, vf_iters=5, vf_stepsize=1e-3)
    total_timesteps = 0
    rewbuffer = deque(maxlen=100) # rolling buffer for episode rewards
    fout = open('res-trpo-relu-%s-%s.txt' % (env_id, seed), 'a+')
    def get_pi_var():
        pi_var_list = []
        for var in tf.trainable_variables():
            if "vf" not in var.name and "old" not in var.name:
                pi_var_list.append(var)
        return pi_var_list
    print( get_pi_var() )
    def getmap():
        pi_var_list = get_pi_var()
        pi_var_names = [v.name for v in pi_var_list]
        pi_var_values = sess.run(pi_var_names)
        pi_var_ = {}
        for i, p in enumerate(pi_var_list):
            pi_var_[p.name] = np.copy(pi_var_values[i])
        return pi_var_

    pi_var_gradient = {}
    #print (trpoLearn.pi.get_variables())
    #print ("---------------------------")
    #print (trpoLearn.oldpi.get_variables())
    pi_var, oldpi_var = [], []
    for var in tf.trainable_variables():
        if "vf" not in var.name and "old" not in var.name:
            pi_var.append(var)
        if "old" in var.name and "vf" not in var.name and "obfilter" not in var.name:
            oldpi_var.append(var)
    print (pi_var)
    print ("---------------------------")
    print (oldpi_var)
    pi_reset = U.function([],[], updates=[tf.assign(newv, oldv)
        for (oldv, newv) in zipsame(oldpi_var, pi_var)])
    assign_old_eq_new = U.function([],[], updates=[tf.assign(oldv, newv)
        for (oldv, newv) in zipsame(oldpi_var, pi_var)])
    #####
    print ("==========================")
    print (tf.trainable_variables())

    while total_timesteps < num_timesteps:
        seg = get_experience(env, trpoLearn.pi, timesteps_per_batch)
        add_vtarg_and_adv(seg, gamma, lam)

        pi_var_before = getmap()
        assign_old_eq_new()  #old <- new, before any update
        timesteps_this_batch, raw_returns = trpoLearn.update_once(seg)
        pi_var_after = getmap()
        for k,v in pi_var_after.items():
            pi_var_gradient[k] = np.copy(pi_var_after[k] - pi_var_before[k])
        for k,v in pi_var_before.items():
            gradient = np.zeros_like(pi_var_before[k])
            gradient = pi_var_gradient[k]
            pi_var_before[k] = pi_var_before[k] + gradient
        pi_reset() #old -> new, after each update
        pi_update = U.function([],[], updates=[tf.assign(p, pi_var_before[p.name])
            for i,p in enumerate(get_pi_var())])
        pi_update()

        total_timesteps += timesteps_this_batch
        rewbuffer.extend(raw_returns)
        fout.write('%d %d\n' % (total_timesteps, np.mean(rewbuffer)))
        fout.flush()
    env.close()

def main():
    import argparse
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--env', help='environment ID', default='HalfCheetah-v1')
    parser.add_argument('--seed', help='RNG seed', type=int, default=0)
    parser.add_argument('--num-timesteps', type=int, default=int(2e6))
    args = parser.parse_args()
    logger.configure()
    train(args.env, num_timesteps=args.num_timesteps, seed=args.seed)


if __name__ == '__main__':
    main()
