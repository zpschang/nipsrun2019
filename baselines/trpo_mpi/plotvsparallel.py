import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import os, sys

files, files1, files2, files3, files4 = [],[],[],[],[]
labels, labels1, labels2, labels3, labels4 = [],[],[],[],[]

plt.ylabel('Return')
plt.xlabel('Timestep x 1e6')
axes = plt.gca()

def moving_avg(a, n=3):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n-1:] / n

#game = 'hopperv1'
#game = 'halfcheetahv1'

#game = 'Swimmer-v1'
#game = 'Hopper-v1'
game = sys.argv[1] + '-v1'
#unify
for i,f in enumerate(os.listdir("./")):
    if 'txt' not in f: continue
    if game in f and 'trpo' in f and int(f.split('-')[-1][:-4]) < 10 and 'relu' in f:
        files1.append(f)
        labels1.append('trpo')
    if game in f and 'ppo' in f and int(f.split('-')[-1][:-4]) < 10:
        files2.append(f)
        labels2.append('ppo')
    #if game in f and 'acktr' in f:
    #    files3.append(f)
    #    labels3.append('acktr')
print (files)
print (files1)
print (files2)
print (files3)

plt.title(game.title()  )

def fill(files_, color_, color1_, label_):
    if len(files_) == 0: return
    agent_used = [[] for _ in range(20)]
    maxs = [[], []]
    mins = [[], []]
    avgs = [[], []]
    alls = []
    leng = 1e8
    for i,f in enumerate(files_):
        fin = open(f, 'r')
        steps = []
        scores = []
        for l in fin:
            t = l.split()
            step = int(t[0])
            score = float(t[1])
            steps.append(step)
            scores.append(score)
        #plt.plot(steps[10-1:], moving_avg(scores, 10), label=f.split('-')[0])
        ##plt.plot(steps[10-1:], moving_avg(scores, 10), label=labels[i])
        alls.append([steps, scores])
        leng = int(min(len(steps), leng))

    for i in range(int(leng)):
        tmp1 = []
        tmp2 = []
        for j in range(len(alls)):
            tmp1.append(alls[j][0][i])
            tmp2.append(alls[j][1][i])
        #maxs[1].append(max(tmp2))
        #mins[1].append(min(tmp2))
        maxs[1].append(np.mean(tmp2) + np.std(tmp2))
        mins[1].append(np.mean(tmp2) - np.std(tmp2))
        avgs[1].append(np.mean(tmp2))
        maxs[0].append(np.mean(tmp1))
        mins[0].append(np.mean(tmp1))
        avgs[0].append(np.mean(tmp1))

    #plt.plot(mins[0], mins[1])
    #plt.plot(maxs[0], maxs[1])
    plt.fill_between(np.array(mins[0])/1e6, mins[1], maxs[1], color=color_, alpha=0.4)
    plt.plot(np.array(avgs[0])/1e6, avgs[1], color1_, label=label_)
    print (agent_used[0].count(0), agent_used[0].count(1))

fill(files, 'salmon', 'red', 'unify')
fill(files1, 'lightgreen', 'green', 'trpo')
fill(files2, 'cyan', 'blue', 'ppo')
fill(files3, 'orchid', 'purple', 'acktr')
#fill(files1, 'grey', 'black', 'trpo')

plt.legend(loc='lower right')
plt.savefig(game + '-vs-parallel.png')


