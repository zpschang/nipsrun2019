import numpy as np

def pathlength(path):
    return path["reward"].shape[0]# Loss function that we'll differentiate to get the policy gradient

ret = 0.
def rollout(env, pi, max_pathlength, timesteps_this_batch, algo='acktr', pi2=None, render=False):
    global ret
    """
    Simulate the env and policy for max_pathlength steps
    """
    ob = env.reset()
    ob = np.array(ob)
    prev_ob = np.float32(np.zeros(ob.shape))
    terminated = False
    gamma = 0.99

    obs = []
    acs = []
    ac_dists = []
    logps = []
    rewards = []
    raw_rew = []
    #trpo
    news = []
    vpreds = []
    ep_rets = []
    ep_lens = []
    cur_ep_ret, cur_ep_len = 0, 0
    #ppo
    mb_neglogpacs = []
    #acktr
    epinfos = []
    timesteps_this_batch_tmp = timesteps_this_batch
    for ii in range(max_pathlength):
        state = np.concatenate([ob, prev_ob], -1)
        #state = ob
        state = np.reshape(state, (-1,))
        if algo == 'trpo':
            pi.ob_rms.update(np.array([state]))
            ob_rms = pi.ob_rms
            state = np.clip((state - ob_rms.mean) / np.sqrt(ob_rms.var + 1e-8), -5.0, 5.0)
            ac, vpred = pi.act(True, state)
            vpreds.append(vpred)
        elif algo == 'acktr':
            pi.ob_rms.update(np.array([ob]))
            ob_rms = pi.ob_rms
            ob = np.clip((ob - ob_rms.mean) / np.sqrt(ob_rms.var + 1e-8), -10.0, 10.0)
            state = np.concatenate([ob, prev_ob], -1)
            #state = ob
            state = np.reshape(state, (-1,))

            ac, ac_dist, logp = pi.act(state) ##TODO
            ac_dists.append(ac_dist)
            logps.append(logp)
        else:
            #pi:model
            ob = np.reshape(ob, (-1,))
            pi.act_model.ob_rms.update(np.array([ob]))
            ob_rms = pi.act_model.ob_rms
            ob = np.clip((ob - ob_rms.mean) / np.sqrt(ob_rms.var + 1e-8), -10.0, 10.0)
            state = np.concatenate([ob, prev_ob], -1)
            #state = ob
            state = np.reshape(state, (-1,))

            ac, vpred, _, neglogpacs, mean, logstd = pi.step(state)
            #ac2, ac_dist2, logp2 = pi2.act(state) ##TODO
            #print (neglogpacs, logp2)
            #print (ac, ac2)
            #print (mean[0][0], np.exp(logstd)[0][0], ac_dist2)
            #ac, vpred, neglogpacs, mean, logstd = ac[0], vpred[0], neglogpacs[0], mean[0], logstd[0]
            ac = np.reshape(ac, (-1,))
            vpred = np.reshape(vpred, (-1,))
            neglogpacs = np.reshape(neglogpacs, (-1,))
            mean = np.reshape(mean, (-1,))
            logstd = np.reshape(logstd, (-1,))
            '''
            ac = np.squeeze(ac)
            vpred = np.squeeze(vpred)
            neglogpacs = np.squeeze(neglogpacs)
            mean = np.squeeze(mean)
            logstd = np.squeeze(logstd)
            '''
            mb_neglogpacs.append(neglogpacs)
            vpreds.append(vpred)
            #mean, logstd = pi.act_model.get_dist(state)

            #ACKTR
            ac_dists.append(np.concatenate([mean, np.exp(logstd)], -1))
            logps.append(-neglogpacs)
        if render: env.render()

        obs.append(state)
        acs.append(ac)
        #ep_values.append(values)
        prev_ob = np.copy(ob)
        #scaled_ac = env.action_space.low + (ac + 1.) * 0.5 * (env.action_space.high - env.action_space.low)
        #scaled_ac = np.clip(scaled_ac, env.action_space.low, env.action_space.high)
        scaled_ac = ac
        ob, rew, done, info = env.step(scaled_ac)
        ob = np.array(ob)
        raw_rew.append(rew)
        if algo == 'ppo':
            ret = ret * gamma + rew
            pi.act_model.ret_rms.update(np.array([ret]))
            rew = np.clip(rew / np.sqrt(pi.act_model.ret_rms.var + 1e-8), -10., 10.)
        ####ppo
        #for info in infos:
        #    maybeepinfo = info.get('episode')
        #    if maybeepinfo:
        #        epinfos.append(maybeepinfo)
        #        ep_rets.append(maybeepinfo['r'])
        #        ep_lens.append(maybeepinfo['l'])
        maybeepinfo = info.get('episode')
        if maybeepinfo:
            epinfos.append(maybeepinfo)
            ep_rets.append(maybeepinfo['r'])
            ep_lens.append(maybeepinfo['l'])
        ####
        rewards.append(rew)
        news.append(done)
        cur_ep_ret += rew
        cur_ep_len += 1
        timesteps_this_batch_tmp += 1
        if timesteps_this_batch_tmp >= max_pathlength: break
        if done:
            terminated = True
            #ep_rets.append(cur_ep_ret)
            #ep_lens.append(cur_ep_len)
            #state = np.concatenate([ob, prev_ob], -1)
            #_, vpred, _, _ = pi.step([state])
            #vpreds.append(vpred)
            break
    obs = np.array(obs)
    acs = np.array(acs)
    rewards = np.array(rewards)
    ep_rets = np.array(ep_rets)
    ep_lens = np.array(ep_lens)
    news = np.array(news)
    vpreds = np.array(vpreds)
    ac_dists = np.array(ac_dists)
    logps = np.array(logps)
    return {"observation" : obs, "terminated" : terminated,
            "reward" : rewards, "action" : acs,
            "action_dist": ac_dists, "logp" : logps,
            "new": news, "ep_rets": ep_rets, "ep_lens": ep_lens,
            "vpred": vpreds, "neglogpacs": mb_neglogpacs, "epinfos": epinfos,
            "raw_ret100": np.sum(raw_rew[:100]), "raw_ret150": np.sum(raw_rew[:150]),
            "raw_ret200": np.sum(raw_rew[:200]), "raw_ret300": np.sum(raw_rew[:300])}

    '''
    discount_ret = 0
    lastgaelam = 0
    discount_rets = []
    gamma = 0.99
    lam = 0.97
    last_values = value_function(state) ##TODO
    advs = np.zeros_like(rewards)
    values = np.array(values)
    rewards = np.array(rewards)
    ep_values = np.array(ep_values)
    ep_rets = np.array(ep_rets)
    ep_lens = np.array(ep_lens)
    obs = np.array(obs)
    acs = np.array(acs)
    ac_dists = np.array(ac_dists)
    logs = np.array(logs)

    for t in reversed(range(max_pathlength)):
        discount_ret = rewards[t] + discount_ret * gamma
        if t == max_pathlength - 1:
            if terminiated == True:
                discount_rets.append(discount_ret)
                discount_ret = 0
            nextnonterminal = 1.0 - terminiated
            nextvalues = last_values
        else:
            if news[t+1] == True:
                discount_rets.append(discount_ret)
                discount_ret = 0
            nextnonterminal = 1.0 - news[t+1]
            nextvalues = ep_values[t+1]
        delta = rewards[t] + gamma * nextvalues * nextnonterminal - ep_values[t]
        advs[t] = lastgaelam = delta + gamma * lam * nextnonterminal * lastgaelam
    returns = advs + values

    vpred = np.append(values, last_values)

    return {"observation" : obs, "terminated" : terminated,
            "reward" : rewards, "action" : acs,
            "action_dist": ac_dists, "logp" : logps,
            "new": news, "ep_rets": ep_rets, "ep_lens": ep_lens,
            "returns": returns,
            "adv": advs, "tdlamret": returns, "vpred": vpred}
    '''









def traj_segment_generator(pi, env, horizon, stochastic):
    # Initialize state variables
    t = 0
    ac = env.action_space.sample()
    new = True
    rew = 0.0
    prev_ob = ob = env.reset()

    cur_ep_ret = 0
    cur_ep_len = 0
    ep_rets = []
    ep_lens = []
    ep_rets_discount = []
    acs_dist = []
    reward = []
    observation = []

    # Initialize history arrays
    state = np.concatenate([ob, ob])
    obs = np.array([state for _ in range(horizon)])
    rews = np.zeros(horizon, 'float32')
    vpreds = np.zeros(horizon, 'float32')
    news = np.zeros(horizon, 'int32')
    acs = np.array([ac for _ in range(horizon)])
    means = np.array([ac for _ in range(horizon)])
    logstds = np.array([ac for _ in range(horizon)])
    prevacs = acs.copy()

    ep_rews = []
    ep_dist = []
    ep_obs = []
    ac_dim = env.action_space.shape[0]


    while True:
        prevac = ac
        state = np.concatenate([ob, prev_ob])
        ac, vpred = pi.act(stochastic, state)
        mean, logstd = pi.get_dist(state)
        #logprobsampled_n = - np.sum(logstd, axis=1) - 0.5 * np.log(2.0*np.pi)*ac_dim - 0.5 * np.sum(np.square(mean - ac) / (np.square(np.exp(logstd))), axis=1) # Logprob of sampled action
        #logprobsampled_n = - U.sum(tf.log(ac_dist[:,ac_dim:]), axis=1) - 0.5 * tf.log(2.0*np.pi)*ac_dim - 0.5 * U.sum(tf.square(ac_dist[:,:ac_dim] - sampled_ac_na) / (tf.square(ac_dist[:,ac_dim:])), axis=1) # Logprob of sampled action
        # Slight weirdness here because we need value function at time T
        # before returning segment [0, T-1] so we get the correct
        # terminal value
        if t > 0 and t % horizon == 0:
            yield {"ob" : obs, "rew" : rews, "vpred" : vpreds, "new" : news,
                    "ac" : acs, "prevac" : prevacs, "nextvpred": vpred * (1 - new),
                    "ep_rets" : ep_rets, "ep_lens" : ep_lens, "ep_rets_discount": ep_rets_discount,
                    "action_dist" : acs_dist, "reward": reward, "observation": observation}  #, "logp" : logprobsampled_n}
            _, vpred = pi.act(stochastic, state)
            # Be careful!!! if you change the downstream algorithm to aggregate
            # several of these batches, then be sure to do a deepcopy
            ep_rets = []
            ep_lens = []
            ep_rets_discount = []
            acs_dist = []
            reward = []
            observation = []
            #NOTE: add reset!!!
            #ob = prev_ob = env.reset()
            #state = np.concatenate([ob, prev_ob])

        i = t % horizon
        obs[i] = state
        prev_ob = ob
        vpreds[i] = vpred
        news[i] = new
        acs[i] = ac
        prevacs[i] = prevac

        ob, rew, new, _ = env.step(ac)
        #print (rew)
        rews[i] = rew

        cur_ep_ret += rew
        ep_rews.append(rew)

        ep_dist.append([mean, logstd])
        ep_obs.append(state)

        cur_ep_len += 1
        if new:
            ep_rets.append(cur_ep_ret)
            ep_lens.append(cur_ep_len)
            discount_ret = 0
            for j in reversed(range(len(ep_rews))):
                discount_ret = ep_rews[j] + 0.99 * discount_ret
            ep_rets_discount.append(discount_ret)

            acs_dist.append(np.array(ep_dist))
            reward.append(np.array(ep_rews))
            observation.append(np.array(ep_obs))

            ep_rews = []
            ep_obs = []
            ep_dist = []

            cur_ep_ret = 0
            cur_ep_len = 0
            ob = prev_ob = env.reset()
            state = np.concatenate([ob, prev_ob])
        t += 1




class Runner(object):

    def __init__(self, *, env, model, nsteps, gamma, lam):
        self.env = env
        self.model = model
        nenv = env.num_envs
        self.obs = np.zeros((nenv,) + (env.observation_space.shape[0]*2,), dtype=model.train_model.X.dtype.name)
        init_ob = self.ob = self.prev_ob = env.reset()
        self.obs[:] = np.concatenate([init_ob, init_ob], axis=1)
        self.gamma = gamma
        self.lam = lam
        self.nsteps = nsteps
        self.states = model.initial_state
        self.dones = [False for _ in range(nenv)]

    def run(self):
        mb_obs, mb_rewards, mb_actions, mb_values, mb_dones, mb_neglogpacs = [],[],[],[],[],[]
        mb_states = self.states
        epinfos = []

        reward, action_dist, observation = [], [], []
        reward_ep, action_dist_ep, observation_ep = [], [], []

        for _ in range(self.nsteps):
            actions, values, self.states, neglogpacs = self.model.step(self.obs, self.states, self.dones)

            observation_ep.append(self.obs)
            mean, logstd = self.model.act_model.get_dist(self.obs)
            action_dist_ep.append([mean, logstd])

            mb_obs.append(self.obs.copy())
            mb_actions.append(actions)
            mb_values.append(values)
            mb_neglogpacs.append(neglogpacs)
            mb_dones.append(self.dones)            
            #self.obs[:], rewards, self.dones, infos = self.env.step(actions)
            self.ob, rewards, self.dones, infos = self.env.step(actions)

            reward_ep.append(rewards)

            for info in infos:
                maybeepinfo = info.get('episode')
                if maybeepinfo: epinfos.append(maybeepinfo)
            mb_rewards.append(rewards)

            if self.dones:
                self.prev_ob = self.ob
                self.obs[:] = np.concatenate([self.ob, self.prev_ob], axis=1)
                reward.append(np.array(reward_ep))
                action_dist.append(np.array(action_dist_ep))
                observation.append(np.array(observation_ep))
                reward_ep, action_dist_ep, observation_ep = [], [], []
            else:
                self.obs[:] = np.concatenate([self.ob, self.prev_ob], axis=1)
                self.prev_ob = self.ob

        #batch of steps to batch of rollouts
        mb_obs = np.asarray(mb_obs, dtype=self.obs.dtype)
        mb_rewards = np.asarray(mb_rewards, dtype=np.float32)
        mb_actions = np.asarray(mb_actions)
        mb_values = np.asarray(mb_values, dtype=np.float32)
        mb_neglogpacs = np.asarray(mb_neglogpacs, dtype=np.float32)
        mb_dones = np.asarray(mb_dones, dtype=np.bool)
        last_values = self.model.value(self.obs, self.states, self.dones)
        #discount/bootstrap off value fn
        mb_returns = np.zeros_like(mb_rewards)
        mb_returns_discount = []
        mb_advs = np.zeros_like(mb_rewards)
        lastgaelam = 0
        discount_ret = 0
        for t in reversed(range(self.nsteps)):
            discount_ret = mb_rewards[t] + discount_ret * self.gamma
            if t == self.nsteps - 1:
                if self.dones == True:
                    mb_returns_discount.append(discount_ret)
                    discount_ret = 0
                nextnonterminal = 1.0 - self.dones
                nextvalues = last_values
            else:
                if mb_dones[t+1] == True:
                    mb_returns_discount.append(discount_ret)
                    discount_ret = 0
                nextnonterminal = 1.0 - mb_dones[t+1]
                nextvalues = mb_values[t+1]
            delta = mb_rewards[t] + self.gamma * nextvalues * nextnonterminal - mb_values[t]
            mb_advs[t] = lastgaelam = delta + self.gamma * self.lam * nextnonterminal * lastgaelam
        mb_returns = mb_advs + mb_values
        return (*map(sf01, (mb_obs, mb_returns, mb_dones, mb_actions, mb_values, mb_neglogpacs)), 
            mb_states, epinfos, reward, action_dist, observation, mb_returns_discount)
# obs, returns, masks, actions, values, neglogpacs, states = runner.run()
