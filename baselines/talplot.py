import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import os, sys

files, files1, files2, files3, files4 = [],[],[],[],[]
labels, labels1, labels2, labels3, labels4 = [],[],[],[],[]
files5, labels5 = [], []
files6, labels6 = [], []
files7, labels7 = [], []
files8, labels8 = [], []
files9, labels9 = [], []
files10, labels10 = [], []
files11, labels11 = [], []

plt.ylabel('Return')
plt.xlabel('Timestep x 1e6')
axes = plt.gca()

def moving_avg(a, n=3):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n-1:] / n

#game = 'hopperv1'
#game = 'halfcheetahv1'

#game = 'Swimmer-v1'
#game = 'Hopper-v1'
game = sys.argv[1]
#unify
files = {}
folder = "./fout-results/"
for i,f in enumerate(os.listdir(folder)):
    if 'txt' not in f: continue
    if game in f and 'upo3' in f and 'test2_' in f and int(f.split('-')[-1][:-4]) < 10:
        label = f.split('-')[3]
        if label not in files.keys():
            files[label] = []
        files[label].append(folder + f)
print (files)

plt.title(game.title()  )

def fill(files_, color_, color1_, label_):
    if len(files_) == 0: return
    agent_used = [[] for _ in range(20)]
    maxs = [[], []]
    mins = [[], []]
    avgs = [[], []]
    alls = []
    leng = 1e8
    for i,f in enumerate(files_):
        fin = open(f, 'r')
        steps = []
        scores = []
        for l in fin:
            t = l.split()
            step = int(t[0])
            score = float(t[1])
            steps.append(step)
            scores.append(score)
        #plt.plot(steps[10-1:], moving_avg(scores, 10), label=f.split('-')[0])
        ##plt.plot(steps[10-1:], moving_avg(scores, 10), label=labels[i])
        alls.append([steps, scores])
        leng = int(min(len(steps), leng))

    for i in range(int(leng)):
        tmp1 = []
        tmp2 = []
        for j in range(len(alls)):
            tmp1.append(alls[j][0][i])
            tmp2.append(alls[j][1][i])
        #maxs[1].append(max(tmp2))
        #mins[1].append(min(tmp2))
        maxs[1].append(np.mean(tmp2) + np.std(tmp2))
        mins[1].append(np.mean(tmp2) - np.std(tmp2))
        avgs[1].append(np.mean(tmp2))
        maxs[0].append(np.mean(tmp1))
        mins[0].append(np.mean(tmp1))
        avgs[0].append(np.mean(tmp1))

    #plt.plot(mins[0], mins[1])
    #plt.plot(maxs[0], maxs[1])
    plt.fill_between(np.array(mins[0])/1e6, mins[1], maxs[1], color=color_, alpha=0.4)
    plt.plot(np.array(avgs[0])/1e6, avgs[1], color1_, label=label_)
    print (agent_used[0].count(0), agent_used[0].count(1))

meanc = ['red', 'green', 'blue', 'purple', 'black', 'yellow', 'brown']
stdc = ['salmon', 'lightgreen', 'cyan', 'orchid', 'grey', 'khaki', 'sienna']
for i,k in enumerate(files.keys()):
    fill(files[k], stdc[i], meanc[i], k)

plt.legend(loc='lower right')
plt.savefig(game + '-tal.png')


