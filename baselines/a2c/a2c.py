import os.path as osp
import gym
import time
import joblib
import logging
import numpy as np
import tensorflow as tf
from baselines import logger

from baselines.common import set_global_seeds, explained_variance
from baselines.common.vec_env.subproc_vec_env import SubprocVecEnv

from baselines.a2c.utils import discount_with_dones
from baselines.a2c.utils import Scheduler, make_path, find_trainable_variables
from baselines.a2c.policies import CnnPolicy
from baselines.a2c.utils import cat_entropy, mse

class Model(object):

    def __init__(self, policy, ob_space, ac_space, nenvs, nsteps, nstack, num_procs,
            ent_coef=0.01, vf_coef=0.5, max_grad_norm=0.5, lr=7e-4,
            alpha=0.99, epsilon=1e-5, total_timesteps=int(80e6), lrschedule='linear'):
        config = tf.ConfigProto(allow_soft_placement=True,
                                intra_op_parallelism_threads=num_procs,
                                inter_op_parallelism_threads=num_procs)
        config.gpu_options.allow_growth = True
        #sess = tf.Session(config=config)
        sess = tf.get_default_session()
        nact = ac_space.shape[0]
        nbatch = nenvs*nsteps

        step_model = policy(sess, ob_space, ac_space, 1)
        train_model = policy(sess, ob_space, ac_space, nsteps)

        #A = tf.placeholder(tf.int32, [None])
        A = train_model.pdtype.sample_placeholder([None])
        ADV = tf.placeholder(tf.float32, [None])
        R = tf.placeholder(tf.float32, [None])
        LR = tf.placeholder(tf.float32, [])

        #neglogpac = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=train_model.pi, labels=A)
        neglogpac = train_model.pd.neglogp(A)
        entropy = tf.reduce_mean(train_model.pd.entropy())
        pg_loss = tf.reduce_mean(ADV * neglogpac)
        vf_loss = tf.reduce_mean(mse(tf.squeeze(train_model.vf), R))
        #entropy = tf.reduce_mean(cat_entropy(train_model.pi))
        ####loss = pg_loss - entropy * ent_coef + vf_loss * vf_coef
        loss_vf = vf_loss * vf_coef
        loss_pi = pg_loss - entropy * ent_coef

        #params = find_trainable_variables("model")
        with tf.variable_scope("model"):
            params = tf.trainable_variables()
            params_vf, params_pi = [], []
            for var in params:
                if 'old' in var.name: continue
                if 'vf' in var.name: params_vf.append(var)
                if 'pi' in var.name: params_pi.append(var)
        grads_vf = tf.gradients(loss_vf, params_vf)
        grads_pi = tf.gradients(loss_pi, params_pi)
        if max_grad_norm is not None:
            grads_vf, grad_norm_vf = tf.clip_by_global_norm(grads_vf, max_grad_norm)
            grads_pi, grad_norm_pi = tf.clip_by_global_norm(grads_pi, max_grad_norm)
        grads_vf = list(zip(grads_vf, params_vf))
        grads_pi = list(zip(grads_pi, params_pi))
        with tf.variable_scope("model"):
            trainer = tf.train.AdamOptimizer(learning_rate=LR, epsilon=1e-5, name='a2c')
        _train_vf = trainer.apply_gradients(grads_vf)
        _train_pi = trainer.apply_gradients(grads_pi)

        lr = Scheduler(v=lr, nvalues=total_timesteps, schedule=lrschedule)

        '''
        def train(obs, states, rewards, masks, actions, values):
            advs = rewards - values
            advs = (advs - advs.mean()) / (advs.std() + 1e-8)
            for step in range(len(obs)):
                cur_lr = lr.value()
            td_map = {train_model.X:obs, A:actions, ADV:advs, R:rewards, LR:cur_lr}
            policy_loss, value_loss, policy_entropy, _ = sess.run(
                [pg_loss, vf_loss, entropy, _train],
                td_map
            )
            return policy_loss, value_loss, policy_entropy
        '''

        def train(obs, states, rewards, masks, actions, values):
            advs = rewards - values
            advs = (advs - advs.mean()) / (advs.std() + 1e-8)
            for step in range(len(obs)):
                cur_lr = lr.value()
            td_map = {train_model.X:obs, A:actions, ADV:advs, R:rewards, LR:cur_lr}
            for _ in range(25):
                value_loss, _ = sess.run([vf_loss, _train_vf], td_map)
            policy_loss, policy_entropy, _ = sess.run([pg_loss, entropy, _train_pi], td_map)
            return policy_loss, value_loss, policy_entropy

        def save(save_path):
            ps = sess.run(params)
            make_path(save_path)
            joblib.dump(ps, save_path)

        def load(load_path):
            loaded_params = joblib.load(load_path)
            restores = []
            for p, loaded_p in zip(params, loaded_params):
                restores.append(p.assign(loaded_p))
            ps = sess.run(restores)

        self.train = train
        self.train_model = train_model
        self.step_model = step_model
        self.step = step_model.step
        self.value = step_model.value
        self.initial_state = step_model.initial_state
        self.save = save
        self.load = load
        tf.global_variables_initializer().run(session=sess)

class Learn:
    #def __init__(self, policy, env, seed, nsteps=2048, nstack=1, total_timesteps=int(4e6), vf_coef=0.001, ent_coef=0.0, max_grad_norm=5, lr=3e-4, lrschedule='linear', epsilon=1e-5, alpha=0.99, gamma=0.99, log_interval=1):
    #def __init__(self, policy, env, seed, nsteps=2048, nstack=1, total_timesteps=int(4e6), vf_coef=0.5, ent_coef=0.01, max_grad_norm=0.5, lr=7e-4, lrschedule='linear', epsilon=1e-5, alpha=0.99, gamma=0.99, log_interval=1):  ##PPO
    #def __init__(self, policy, env, seed, nsteps=2048, nstack=1, total_timesteps=int(4e6), vf_coef=0.003, ent_coef=0.01, max_grad_norm=0.5, lr=0.003, lrschedule='linear', epsilon=1e-5, alpha=0.99, gamma=0.99, log_interval=1):  # just work in beginning
    #def __init__(self, policy, env, seed, nsteps=2048, nstack=1, total_timesteps=int(4e6), vf_coef=0.03, ent_coef=0.0, max_grad_norm=0.5, lr=0.03, lrschedule='linear', epsilon=1e-5, alpha=0.99, gamma=0.99, log_interval=1):
    #def __init__(self, policy, env, seed, nsteps=2048, nstack=1, total_timesteps=int(4e6), vf_coef=0.03, ent_coef=0.01, max_grad_norm=0.5, lr=0.03, lrschedule='linear', epsilon=1e-5, alpha=0.99, gamma=0.99, log_interval=1):
    def __init__(self, policy, env, seed, nsteps=2048, nstack=1, total_timesteps=int(4e6), vf_coef=0.03, ent_coef=0.01, max_grad_norm=0.5, lr=0.003, lrschedule='linear', epsilon=1e-5, alpha=0.99, gamma=0.99, log_interval=1):
        #tf.reset_default_graph()
        set_global_seeds(seed)

        nenvs = 1
        ob_space = env.observation_space
        ac_space = env.action_space
        num_procs = 1
        model = Model(policy=policy, ob_space=ob_space, ac_space=ac_space, nenvs=nenvs, nsteps=nsteps, nstack=nstack, num_procs=num_procs, ent_coef=ent_coef, vf_coef=vf_coef,
            max_grad_norm=max_grad_norm, lr=lr, alpha=alpha, epsilon=epsilon, total_timesteps=total_timesteps, lrschedule=lrschedule)

        nbatch = nenvs*nsteps
        self.tstart = time.time()
        self.timesteps_so_far = 0
        self.update = 0
        def update_once(seg):
            self.update += 1
            obs, rewards, actions, values, epinfos = seg["ob"], seg["rew"], seg["ac"], seg["vpred"], seg["epinfos"]
            masks, states = None, None
            policy_loss, value_loss, policy_entropy = model.train(obs, states, rewards, masks, actions, values)
            nseconds = time.time() - self.tstart
            fps = int((self.update*nbatch) / nseconds)
            self.timesteps_so_far += seg["timesteps_this_batch"]

            ev = explained_variance(values, rewards)
            logger.record_tabular("nupdates", self.update)
            logger.record_tabular("total_timesteps", self.timesteps_so_far)
            logger.record_tabular("fps", fps)
            logger.record_tabular("policy_entropy", float(policy_entropy))
            logger.record_tabular("value_loss", float(value_loss))
            logger.record_tabular("policy_loss", float(policy_loss))
            logger.record_tabular("explained_variance", float(ev))
            logger.dump_tabular()

            rew_returns = [epinfo['r'] for epinfo in epinfos]
            timesteps_this_batch = seg["timesteps_this_batch"]
            return timesteps_this_batch, rew_returns
        self.update_once = update_once

