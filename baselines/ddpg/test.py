from osim.env import ProstheticsEnv
import numpy as np

env = ProstheticsEnv(visualize=False)
observation = env.reset()
print (env.observation_space)
for i in range(200):
    print (np.array(observation).shape)
    observation, reward, done, info = env.step(env.action_space.sample())
