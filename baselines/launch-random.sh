#!/bin/bash
env=$1
algo=$2
tal=$3
ntest=$4
mkdir logs &> /dev/null
for seed in {0..4}
do
    nohup python run_upo_mujoco.py --env ${env} --seed ${seed} --method ${algo} --tal ${tal} --ntest ${ntest} --random True &> logs/${env}_${seed}_${algo}_test2_${ntest}_${tal}_random &
done
