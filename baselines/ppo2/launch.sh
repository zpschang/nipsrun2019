#!/bin/bash
env=$1
for seed in {0..9}
do
    nohup python run_mujoco.py --env ${env} --seed ${seed} &> ${env}_${seed}_ppo &
done
