import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import os, sys


plt.ylabel('Gradient L2-Norm', fontsize=20)
plt.xlabel('Iteration', fontsize=20)
#axes = plt.gca()

#files = "./fout-results/res-upo3-relu-test2_100_4.00-HalfCheetah-v1-0.txt"
#files = "./fout-results/res-upo3-relu-test2_100_4.00-HalfCheetah-v1-4.txt"
files = "./fout-results/res-upo4-relu-test2_200_6.00-HalfCheetah-v1-4.txt"
fin = open(files ,'r')

tp = []
ta = []
pa = []
a2c = []
upo = []
for l in fin:
    cos = l.split()[4].split(',')
    tp.append(float(cos[0]))
    ta.append(float(cos[1]))
    pa.append(float(cos[2]))
    if len(cos) == 4:
        upo.append(float(cos[3]))
    if len(cos) == 5:
        a2c.append(float(cos[3]))
        upo.append(float(cos[4]))
    
plt.title('HalfCheetah'.title(), fontsize=25)

plt.plot(range(len(tp)), tp, label='TRPO')
plt.plot(range(len(ta)), ta, label='PPO')
plt.plot(range(len(pa)), pa, label='ACKTR')
if len(a2c) > 0: plt.plot(range(len(a2c)), a2c, label='A2C')
plt.plot(range(len(upo)), upo, label='UPO')

print (pa)
#plt.yticks(np.arange(0, 1.0, step=0.1))
plt.ylim(ymin=0.0)
#print (np.arange(0, 1.1, step=0.1))

#plt.legend(loc='lower right', prop={'size': 20})
plt.legend(loc='lower right', fontsize=13)
plt.savefig('HalfCheetah-length-upo4.png')


