import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import os, sys


plt.ylabel('Gradient L2-Norm', fontsize=20)
plt.xlabel('Iteration', fontsize=20)
#axes = plt.gca()

#files = "./fout-results/res-upo3-relu-test2_100_4.00-HalfCheetah-v1-0.txt"
#files = "./fout-results/res-upo3-relu-test2_100_4.00-HalfCheetah-v1-4.txt"
#files = "./fout-results/res-upo4-relu-test2_200_6.00-HalfCheetah-v1-4.txt"
files = "./fout-results/res-a2c-relu-test2_100_1.00-HalfCheetah-v1-2.txt"
fin = open(files ,'r')

a2c = []
for l in fin:
    cos = l.split()[3].split(',')
    a2c.append(float(cos[0]))
print (a2c)
    
plt.title('HalfCheetah'.title(), fontsize=25)

if len(a2c) > 0: plt.plot(range(len(a2c)), a2c, label='A2C')

#plt.yticks(np.arange(0, 1.0, step=0.1))
plt.ylim(ymin=0.0)
#print (np.arange(0, 1.1, step=0.1))

#plt.legend(loc='lower right', prop={'size': 20})
plt.legend(loc='lower right', fontsize=13)
plt.savefig('HalfCheetah-length-a2c.png')


